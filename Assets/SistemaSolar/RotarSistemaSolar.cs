using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotarSistemaSolar : MonoBehaviour
{

    public float velocidadAngular = 1;
    
    void Update()
    {
        transform.Rotate(0, velocidadAngular, 0);
    }
}
